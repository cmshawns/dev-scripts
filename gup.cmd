@echo off
setlocal
set buildlog=gitup_build.log
set mergelog=gitup_merge.log

for /f "usebackq tokens=*" %%i in (`git symbolic-ref --short -q HEAD`) do set currentBranch=%%i

echo Getting latest for master...
git checkout master
git pull
if "%errorlevel%" gtr "0" goto end

git checkout %currentBranch%

echo Merging master to %currentBranch%...
git merge master > %mergelog%
if "%errorlevel%" gtr "0" goto merge_error

echo Building project...
if exist "build.proj" (
    msbuild build.proj > %buildlog%
) else (
	echo build.proj file not found.
    goto build_error
)
if "%errorlevel%" gtr "0" goto build_error

powershell get-content %buildlog% -tail 5
goto end

:build_error
echo ERROR - build failed.
if exist gitup_build.log (
	powershell get-content %buildlog% -tail 10
)
goto end

:merge_error
echo ERROR - merge failed.
if exist gitup_merge.log (
	powershell get-content %mergelog% -tail 10
)
goto end

:end
endlocal

Shawn's dev tools (i.e. scripts, etc.)
======================================

`np` - _open Notepad_ (or editor registered with Windows)

## Git

`gbl.cmd` - _git branch -l_

> Displays branches in current repository.

`gco.cmd` - _git checkout_

> Switches to specified branch.

`gckm.cmd` - _git checkout_ then _merge_

> Switches to the specified branch, then merges from the one that was just left.

`gup.cmd` - _git update_

> Switches to `master`, pulls from the default remote, switches back to previous
> branch and merges `master`.

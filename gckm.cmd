@echo off
setlocal
set destinationBranch=%1

if "%destinationBranch%"=="" goto instructions

for /f "usebackq tokens=*" %%i in (`git symbolic-ref --short -q HEAD`) do set originalBranch=%%i

echo Checking out %1...
git checkout %1
if "%errorlevel%" gtr "0" goto end

echo Merging %originalBranch% to %1...
git merge %originalBranch%
goto end

:instructions
echo.
echo Git checkout, then merge.
echo.
echo    Usage:  gckm branchname
echo.
echo Git will check out branchname, then merge from the branch previously
echo at into the branch just checked out.
echo.
goto end

:end
endlocal
